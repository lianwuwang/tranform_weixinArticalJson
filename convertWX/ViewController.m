//
//  ViewController.m
//  convertWX
//
//  Created by xiang wang on 2018/7/25.
//  Copyright © 2018年 xiang wang. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSData *JSONData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"json" ofType:@"json"]];
    
    
    
    NSArray *dataArr = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingAllowFragments error:nil];
    
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:10];
    
    for (NSDictionary *dataDic in dataArr) {
        NSString *msg = dataDic[@"general_msg_list"];
        NSData *dataStr = [msg dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *dataDic2 = [NSJSONSerialization JSONObjectWithData:dataStr options:NSJSONReadingAllowFragments error:nil];
        
        
        
        for (NSDictionary *detailModel in dataDic2[@"list"]) {
            
            NSString *titleStr = detailModel[@"app_msg_ext_info"][@"title"];
            
            NSDictionary * dic = nil;
            
            if (!titleStr || titleStr.length == 0) {
                for (NSDictionary *subDetailModel in detailModel[@"app_msg_ext_info"][@"multi_app_msg_item_list"]) {
                    dic = @{
                            @"title": subDetailModel[@"title"],
                            @"imgUrl":subDetailModel[@"cover"],
                            @"linkurl":subDetailModel[@"content_url"]
                            };
                    
                    if (dic) {
                        [arr addObject:dic];
                    }
                }
                
            }else{
                dic = @{
                        @"title": detailModel[@"app_msg_ext_info"][@"title"],
                        @"imgUrl":detailModel[@"app_msg_ext_info"][@"cover"],
                        @"linkurl":detailModel[@"app_msg_ext_info"][@"content_url"]
                        };
                if (dic) {
                    [arr addObject:dic];
                }
                
            }
            
        }
        
        
    }
    
    NSError * error = nil;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:&error];
    NSString * jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSMutableString *responseString = [NSMutableString stringWithString:jsonStr];
    NSString *character = nil;
    for (int i = 0; i < responseString.length; i ++) {
        character = [responseString substringWithRange:NSMakeRange(i, 1)];
        if ([character isEqualToString:@"\\"])
            [responseString deleteCharactersInRange:NSMakeRange(i, 1)];
    }

    //直接将此处打印的内容,完整的复制到knowledge.js中,全部替换原先的内容即可
    
    NSLog(@"const knowledgeDataList = %@;\nexport {knowledgeDataList};",responseString);
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
